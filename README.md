<!-- markdownlint-disable MD041 -->
<table style="width:100%; border-collapse:inherit; border:grey outset 1ch; border-radius:4ch;">
<th>Contents:</th><th>License:</th>
<tr>
<td>

- [About:](#about)
- [Usage:](#usage)
  - [Using <kbd>root</kbd>:](#using-kbdrootkbd)
  - [Using <kbd>prepend</kbd>:](#using-kbdprependkbd)
  - [Using <kbd>append</kbd>:](#using-kbdappendkbd)
  - [Using <kbd>Alias_debug</kbd>:](#using-kbdaliasdebugkbd)

</td>
<td style="text-align:center;">

[![License img]][License]\
This work is licensed under a [Creative Commons Attribution-NonCommercial 4.0 International License][License].\
(C) 2020+ [©TriMoon™ ![GitLab img]][Author web], All rights reserved.
</td>
</tr>
</table>

[License img]: https://i.creativecommons.org/l/by-nc/4.0/88x31.png
[License]: https://creativecommons.org/licenses/by-nc/4.0/
        "Creative Commons Attribution-NonCommercial 4.0 International License"
[Author web]: https://gitlab.com/TriMoon
        "TriMoon on GitLab"
[GitLab img]: https://about.gitlab.com/ico/favicon-16x16.png

<!-- omit in toc -->
# setAlias

Include this plugin as **first** in your _plugins definition_ for maximum benefit because it adds functionality that can be used by other plugins.

- Example:

   ```sh
   plugins=(
      setAlias
      ⋯
   )
   ⋯
   source $ZSH/oh-my-zsh.sh
   ```

## About:

This plugin aids in creating or modifying aliases.\
This plugin introduces:

1. `setAlias` as function.
2. `Alias` as an _alias_ to the above function.

## Usage:

<kbd>Alias 'cmd' 'replacement' [operation mode]</kbd>\
Take note of the capital `A` to not get confused with the build-in `alias` of shells⋯

- Where:
  1. `cmd` (Required)\
     The alias you want to define.
  2. `replacement` (Required)\
     The replacement you want the alias to expand to.
  3. `operation mode` (Optional)\
     Can be one **or** two words.
     1. First word only;
        - <kbd>root</kbd> ⇒ Always add <kbd>_root</kbd> infront of `replacement`.\
          You should make sure that you have an alias `_root` defined at some point.\
          Most commonly used is something like <kbd>alias _root=sudo</kbd>.
     2. First or second word;
        - <kbd>pre</kbd>* ⇒ **Prepend** `replacement` to current alias.
        - <kbd>app</kbd>* ⇒ **Append** `replacement` to current alias.
        - _Not provided_ ⇒ **Replace** old alias or create a new one.

- Debug:\
You can set <kbd>Alias_debug</kbd> to a non-zero integer prior to calling the function to get debug output of what it does.\
This is a **4-bit binary flag** to select which step(s) to show, eg **4 steps** in total.

### Using <kbd>root</kbd>:

```sh
⋯
source $ZSH/oh-my-zsh.sh
⋯
# Apache's a2* commands
Alias 'a2enconf'		'a2enconf'	root
Alias 'a2enmod'			'a2enmod'	root
Alias 'a2ensite'		'a2ensite'	root
Alias 'a2disconf'		'a2disconf'	root
Alias 'a2dismod'		'a2dismod'	root
Alias 'a2dissite'		'a2dissite'	root
```

### Using <kbd>prepend</kbd>:

```sh
⋯
source $ZSH/oh-my-zsh.sh
⋯
# ll was `ls -lh`
Alias 'll'				'sudo ' prepend
# ll is now `sudo ls -lh`

# ll was `ls -lh`
Alias 'll'				'sudo ' root prepend
# ll is now `_root sudo ls -lh`
```

### Using <kbd>append</kbd>:

```sh
⋯
source $ZSH/oh-my-zsh.sh
⋯
# ll was `ls -lh`
Alias 'll'				'a --group-directories-first' append
# ll is now `ls -lha --group-directories-first`

# ll was `ls -lh`
Alias 'll'				'a --group-directories-first' root append
# ll is now `_root ls -lha --group-directories-first`
```

### Using <kbd>Alias_debug</kbd>:

```sh
⋯
source $ZSH/oh-my-zsh.sh
⋯
# Single command debug flag (in binary).
# This line ends with a backslash to ease commenting-out :)
Alias_debug=$((2#1010)) \
Alias '_root'				'sudo '

# Global debug flag (in binary) spaning multiple invocations.
Alias_debug=$((2#1010))
Alias '_root'				'sudo '
⋯
Alias_debug=0
```
